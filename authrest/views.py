from websauna.system.http import Request
from websauna.system.core.route import simple_route


# Configure view named home at path / using a template authrest/home.html
@simple_route("/", route_name="home", renderer='authrest/home.html')
def home(request: Request):
    """Render site homepage."""
    return {"project": "AuthREST"}

#@simple_route("/calc_rain_chance", route_name="calc_rain_chance", renderer="json", permission="authenticated")
@simple_route("/api/calc_rain_chance", route_name="calc_rain_chance", renderer="json", require_csrf=False, permission="authenticated")
def calc_rain_chance(request: Request):

    import random

    chance = random.uniform(0.0, 1.0)
    return chance

@simple_route("/api/register_user", route_name="register_user", renderer="json", require_csrf=False)
def register_user(request: Request):

    from websauna.system.user.utils import get_registration_service
    registration_service = get_registration_service(request)

    registration_service.sign_up(user_data=request.POST)

    return True

@simple_route("/api/activate_user", route_name="activate_user", renderer="json", require_csrf=False)
def activate_user(request: Request):

    from websauna.system.user.utils import get_user_registry
    from websauna.system.user.utils import get_registration_service
    user_registry = get_user_registry(request)
    registration_service = get_registration_service(request)

    user = user_registry.get_by_email(request.POST['email'])
    activation_code, expiration_seconds = user_registry.create_email_activation_token(user)

    registration_service.activate_by_email(activation_code)

    return True

@simple_route("/api/login_user", route_name="login_user", renderer="json", require_csrf=False)
def login_user(request: Request):

    from websauna.system.user.utils import get_login_service
    login_service = get_login_service(request)

    login_service.authenticate_credentials(request.POST['email'], request.POST['password'], 'api')

    return True


@simple_route("/api/logout_user", route_name="logout_user", renderer="json", require_csrf=False)
def logout_user(request: Request):
    from websauna.system.user.utils import get_login_service
    login_service = get_login_service(request)

    login_service.logout()

    return True